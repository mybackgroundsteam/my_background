package maestro.my.backgrounds;

import android.app.Application;

import com.dream.android.mim.MIM;
import com.dream.android.mim.MIMCache;
import com.dream.android.mim.MIMInternetMaker;
import com.dream.android.mim.MIMManager;

import java.io.IOException;
import java.io.InputStream;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;

import maestro.my.backgrounds.data.DownloadManager;
import maestro.my.backgrounds.data.Parser;

/**
 * Created by Artyom on 8/30/2015.
 */
public class BApplication extends Application {

    public static final String MIM_INTERNET = "mim_internet";

    @Override
    public void onCreate() {
        super.onCreate();
        MHelper.getInstance().initialize(this);
        MHelper.getInstance().startTrafficMonitor();
        MIMCache.getInstance().setSize(MIMCache.partOfRuntime(6));
        Parser.initialize(this);
        DownloadManager.getInstance().initialize(this);
        MIMManager.getInstance().addMIM(MIM_INTERNET, new MIM(this).maker(new MIMInternetMaker(true) {
            @Override
            public InputStream getNetworkStream(String path) throws IOException {
                try {
                    return Parser.getStream(path);
                } catch (UnrecoverableKeyException e) {
                    e.printStackTrace();
                } catch (NoSuchAlgorithmException e) {
                    e.printStackTrace();
                } catch (KeyStoreException e) {
                    e.printStackTrace();
                } catch (KeyManagementException e) {
                    e.printStackTrace();
                }
                return null;
            }
        }).setThreadCount(3));
    }

}