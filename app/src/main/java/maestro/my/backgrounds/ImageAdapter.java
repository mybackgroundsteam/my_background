package maestro.my.backgrounds;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.dream.android.mim.ImageLoadObject;
import com.dream.android.mim.MIM;
import com.dream.android.mim.OnPreLoadListener;
import com.dream.android.mim.RecyclingImageView;

import maestro.my.backgrounds.data.Image;
import maestro.my.backgrounds.data.Parser;
import maestro.my.backgrounds.data.Size;

/**
 * Created by Artyom on 8/30/2015.
 */
public class ImageAdapter extends RecyclerView.Adapter<ImageAdapter.Holder> implements View.OnClickListener, View.OnLongClickListener {

    public static final int TYPE_NORMAL = 0;
    public static final int TYPE_LOAD_INDICATOR = 1;

    private Context mContext;
    private LayoutInflater mInflater;
    private OnItemClickListener mListener;
    private Image[] mImages;
    private boolean isHasMore = true;
    private int mSize;
    private int mSpanCount;
    private int mMaxBindPosition = -1;

    interface OnItemClickListener {
        void onItemClick(Image image, int position);
    }

    public ImageAdapter(Context context, int spanCount) {
        mContext = context;
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mSize = context.getResources().getDisplayMetrics().widthPixels / spanCount;
        mSpanCount = spanCount;
    }

    public ImageAdapter(Context context, int spanCount, OnItemClickListener listener) {
        this(context, spanCount);
        mListener = listener;
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        mListener = listener;
    }

    public void update(Image[] images) {
        mImages = images;
        notifyDataSetChanged();
    }

    public void setHasMore(boolean hasMore) {
        if (isHasMore != hasMore) {
            isHasMore = hasMore;
            notifyDataSetChanged();
        }
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup viewGroup, int i) {
        Holder holder = new Holder(mInflater.inflate(i == TYPE_NORMAL ? R.layout.image_item_view : R.layout.item_progress_view, null), i);
        if (i == TYPE_NORMAL) {
            holder.itemView.setTag(holder);
            holder.itemView.setOnClickListener(this);
            holder.itemView.setOnLongClickListener(this);
        }
        return holder;
    }

    @Override
    public void onBindViewHolder(final Holder holder, int i) {
        if (getItemViewType(i) == TYPE_NORMAL) {
            Image img = mImages[i];
            holder.RImage.setLoadObject(
                    MIM.by(BApplication.MIM_INTERNET).to(holder.RImage, img.getThumbUrl()).preLoadListener(new OnPreLoadListener() {
                        @Override
                        public void onPreLoad(ImageLoadObject imageLoadObject) {
                            Size size = Parser.adjustSize(imageLoadObject.getWidth(), imageLoadObject.getHeight(), false);
                            if (size != null) {
                                imageLoadObject.path(Image.getAlternateUrl(imageLoadObject.getPath(), size));
                            }
                        }
                    }));
//            holder.mFlowLayout.setVisibility(View.GONE);
//            if (mMaxBindPosition < i) {
//                mMaxBindPosition = i;
//                ObjectAnimator animator = ObjectAnimator.ofFloat(holder.itemView, View.TRANSLATION_Y, holder.itemView.getMeasuredHeight(), 0);
//                animator.setStartDelay(i % mSpanCount * 75);
//                animator.start();
//            }
        }
    }

    @Override
    public void onClick(View v) {
        Holder holder = (Holder) v.getTag();
        if (mListener != null && holder != null) {
            mListener.onItemClick(mImages[holder.getAdapterPosition()], holder.getAdapterPosition());
        }
    }

    @Override
    public boolean onLongClick(View v) {
//        final Holder holder = (Holder) v.getTag();
//        if (holder != null) {
//            Image image = mImages[holder.getAdapterPosition()];
//            if (!TextUtils.isEmpty(image.getTitle())) {
//                String[] splited = image.getTitle().split(",");
//                while (holder.mFlowLayout.getChildCount() - 1 > splited.length) {
//                    holder.mFlowLayout.removeViewAt(0);
//                }
//                for (int i = 0; i < splited.length; i++) {
//                    View child = holder.mFlowLayout.getChildAt(i);
//                    if (child == null) {
//                        holder.mFlowLayout.addView(prepareTagItem(null, splited[i]));
//                    } else {
//                        prepareTagItem(child, splited[i]);
//                    }
//                }
//                ObjectAnimator animator = ObjectAnimator.ofFloat(holder.mFlowLayout, View.ALPHA, 0f, 1f);
//                animator.addListener(new AnimatorListenerAdapter() {
//                    @Override
//                    public void onAnimationStart(Animator animation) {
//                        super.onAnimationStart(animation);
//                        holder.mFlowLayout.setVisibility(View.VISIBLE);
//                    }
//                });
//                animator.start();
//                return true;
//            }
//        }
        return false;
    }

    private View prepareTagItem(View convert, String text) {
        TextView textView;
        if (convert == null) {
            textView = (TextView) mInflater.inflate(R.layout.tag_view, null);
        } else {
            textView = (TextView) convert;
        }
        textView.setText(text);
        return textView;
    }

    public int getImagesCount() {
        return mImages != null ? mImages.length : 0;
    }

    @Override
    public int getItemCount() {
        return isHasMore ? getImagesCount() + 1 : getImagesCount();
    }

    @Override
    public int getItemViewType(int position) {
        return isHasMore && position == getImagesCount() ? TYPE_LOAD_INDICATOR : TYPE_NORMAL;
    }

    public class Holder extends RecyclerView.ViewHolder {

        RecyclingImageView RImage;
//        FlowLayout mFlowLayout;

        public Holder(View itemView, int type) {
            super(itemView);
            if (type == TYPE_NORMAL) {
                RImage = (RecyclingImageView) itemView.findViewById(R.id.image);
//                mFlowLayout = (FlowLayout) itemView.findViewById(R.id.tags_layout);
                RImage.setHasFixedSize(true);
                RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) RImage.getLayoutParams();
                params.width = mSize;
                params.height = mSize;
                itemView.setTag(this);
            }
        }
    }

}