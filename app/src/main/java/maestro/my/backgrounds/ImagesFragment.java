package maestro.my.backgrounds;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.AsyncTaskLoader;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.dream.android.mim.MIM;
import com.dream.android.mim.RecyclingImageView;

import maestro.my.backgrounds.data.Category;
import maestro.my.backgrounds.data.Image;
import maestro.my.backgrounds.data.Parser;

/**
 * Created by Artyom on 8/29/2015.
 */
public class ImagesFragment extends LoadableFragment implements LoaderManager.LoaderCallbacks<Object>,
        ImageAdapter.OnItemClickListener {

    public static final String TAG = ImagesFragment.class.getSimpleName();

    private static final int LOAD_OFFSET = 5;

    public static final ImagesFragment makeInstance(int id) {
        ImagesFragment fragment = new ImagesFragment();
        Bundle args = new Bundle(1);
        args.putInt(PARAM_ID, id);
        fragment.setArguments(args);
        return fragment;
    }

    private RecyclerView mList;
    private GridLayoutManager mLayoutManager;
    private ImageAdapter mAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View v = inflater.inflate(getLayoutId(), null);
        mList = (RecyclerView) v.findViewById(R.id.list);
        mList.setLayoutManager(mLayoutManager = new GridLayoutManager(getActivity(), getSpanCount()) {
//            @Override
//            protected int getExtraLayoutSpace(RecyclerView.State state) {
//                return getResources().getDisplayMetrics().heightPixels;
//            }
        });
        mAdapter = new ImageAdapter(getActivity(), getSpanCount(), this);
        mList.setAdapter(mAdapter);
        mList.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (!isLoading()) {
                    int lastPosition = mList.getChildAdapterPosition(mList.getChildAt(mList.getChildCount() - 1));
                    if (lastPosition >= mAdapter.getItemCount() - 1 - LOAD_OFFSET && lastPosition <= mAdapter.getItemCount() - 1) {
                        loadItems();
                    }
                }
            }
        });
        mLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                return mAdapter.getItemViewType(position) == ImageAdapter.TYPE_LOAD_INDICATOR ? getSpanCount() : 1;
            }
        });
        mList.addItemDecoration(new SpaceItemDecoration(getSpanCount(),
                (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 2, getResources().getDisplayMetrics()), 0, false));
        return v;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mAdapter.update(getCategory().getList().getImages());
        if (mAdapter.getImagesCount() == 0) {
            loadItems();
        }
    }

    @Override
    public void reloadItems() {
        mAdapter.update(null);
        super.reloadItems();
    }

    private int getSpanCount() {
        return getResources().getInteger(R.integer.column_count);
    }

    protected int getLayoutId(){
        return R.layout.images_fragment_view;
    }

    @Override
    public String getKey() {
        return TAG;
    }

    @Override
    public void onUpdate(Image[] images, boolean hasMore) {
        mAdapter.update(images);
        mAdapter.setHasMore(hasMore);
    }

    @Override
    public void onItemClick(Image image, int position) {
        ImagesPagerFragment.makeInstance(getCategory().getId(), position).show(getChildFragmentManager(), ImagesPagerFragment.TAG);
    }
}