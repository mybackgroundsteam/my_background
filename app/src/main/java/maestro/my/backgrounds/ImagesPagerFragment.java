package maestro.my.backgrounds;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import maestro.my.backgrounds.data.Category;
import maestro.my.backgrounds.data.Image;
import maestro.my.backgrounds.data.Parser;

/**
 * Created by Artyom on 8/30/2015.
 */
public class ImagesPagerFragment extends LoadableFragment {

    public static final String TAG = ImagesPagerFragment.class.getSimpleName();

    private static final String PARAM_POSITION = "position";
    private static final int LOADABLE_OFFSET = 2;

    public static final ImagesPagerFragment makeInstance(int id, int position) {
        ImagesPagerFragment fragment = new ImagesPagerFragment();
        Bundle args = new Bundle(2);
        args.putInt(PARAM_ID, id);
        args.putInt(PARAM_POSITION, position);
        fragment.setArguments(args);
        return fragment;
    }

    private ViewPager mPager;
    private ImagesPagerAdapter mAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View v = inflater.inflate(R.layout.images_pager_fragment, null);
        mPager = (ViewPager) v.findViewById(R.id.pager);
        mPager.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                super.onPageSelected(position);
                if (position >= mAdapter.getCount() - 1 - LOADABLE_OFFSET) {
                    loadItems();
                }
            }
        });
        return v;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mPager.setAdapter(mAdapter = new ImagesPagerAdapter(getChildFragmentManager(), getCategory().getList().getImages()));
        mPager.setCurrentItem(getArguments().getInt(PARAM_POSITION));
    }

    @Override
    public String getKey() {
        return TAG;
    }

    @Override
    public boolean isFullscreen() {
        return true;
    }

    @Override
    public void onUpdate(Image[] images, boolean hasMore) {
        mAdapter.update(images);
        mAdapter.setHasNext(hasMore);
    }

    public static final class ImagesPagerAdapter extends FragmentStatePagerAdapter {

        private Image[] mImages;
        private boolean isHasNext = true;

        public ImagesPagerAdapter(FragmentManager fm, Image[] images) {
            super(fm);
            mImages = images;
        }

        public void update(Image[] images) {
            mImages = images;
            notifyDataSetChanged();
        }

        public void setHasNext(boolean hasNext) {
            if (isHasNext != hasNext) {
                isHasNext = hasNext;
                notifyDataSetChanged();
            }
        }

        @Override
        public Fragment getItem(int position) {
            if (position == getImagesCount()) {
                return new LoadPageFragment();
            }
            return SingleImageFragment.makeInstance(mImages[position]);
        }

        @Override
        public int getCount() {
            return isHasNext ? getImagesCount() + 1 : getImagesCount();
        }

        public int getImagesCount() {
            return mImages != null ? mImages.length : 0;
        }

        @Override
        public int getItemPosition(Object object) {
            return POSITION_NONE;
        }
    }

    public static final class LoadPageFragment extends Fragment {
        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            return inflater.inflate(R.layout.load_page_fragment_view, null);
        }
    }

}