package maestro.my.backgrounds;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.AsyncTaskLoader;
import android.support.v4.content.Loader;

import maestro.my.backgrounds.data.Category;
import maestro.my.backgrounds.data.Image;
import maestro.my.backgrounds.data.Parser;

/**
 * Created by Artyom on 8/30/2015.
 */
public abstract class LoadableFragment extends DialogFragment implements LoaderManager.LoaderCallbacks<Object> {

    public static final String PARAM_ID = "id";

    private Category mCategory;
    private boolean isLoading = true;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (isFullscreen()) {
            setStyle(STYLE_NORMAL, R.style.MTheme_Overlay);
        }
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mCategory = Parser.getCategory(getArguments().getInt(PARAM_ID));
    }

    public void reloadItems() {
        loadItems();
    }

    public void loadItems() {
        if (mCategory.getList().hasNext()) {
            getLoaderManager().restartLoader(getKey().hashCode(), null, this);
        }
    }

    @Override
    public Loader<Object> onCreateLoader(int id, Bundle args) {
        isLoading = true;
        return new ImagesLoader(getActivity(), mCategory);
    }

    @Override
    public void onLoadFinished(Loader<Object> loader, Object data) {
        if (data == null) {
            onUpdate(mCategory.getList().getImages(), mCategory.getList().hasNext());
        } else {
            //TODO: display error
        }
        isLoading = false;
    }

    @Override
    public void onLoaderReset(Loader<Object> loader) {

    }

    public Category getCategory() {
        return mCategory;
    }

    public boolean isLoading() {
        return isLoading;
    }

    public boolean isFullscreen() {
        return false;
    }

    public abstract String getKey();

    public abstract void onUpdate(Image[] images, boolean hasMore);

    public static final class ImagesLoader extends AsyncTaskLoader<Object> {

        private Category mCategory;

        public ImagesLoader(Context context, Category category) {
            super(context);
            mCategory = category;
        }

        @Override
        protected void onStartLoading() {
            super.onStartLoading();
            forceLoad();
        }

        @Override
        public Object loadInBackground() {
            return Parser.loadNextImages(mCategory.getList());
        }
    }

}
