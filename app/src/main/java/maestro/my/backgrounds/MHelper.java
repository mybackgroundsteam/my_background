package maestro.my.backgrounds;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.TrafficStats;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;

import java.util.ArrayList;

/**
 * Created by artyom on 6/11/14.
 */
public class MHelper {

    public static final String TAG = MHelper.class.getSimpleName();

    private static volatile MHelper instance;

    public static synchronized MHelper getInstance() {
        MHelper localInstance = instance;
        if (localInstance == null) {
            synchronized (MHelper.class) {
                localInstance = instance;
                if (localInstance == null) {
                    localInstance = instance = new MHelper();
                }
            }
        }
        return localInstance;
    }

    MHelper() {
    }

    private Context mContext;
    private ArrayList<OnConnectionChangeListener> mListeners = new ArrayList<>();
    private boolean isNetworkWiFi;
    private boolean isNetworkMobile;
    private IntentFilter mFilter = new IntentFilter();

    {
        mFilter.addAction("android.net.conn.CONNECTIVITY_CHANGE");
        mFilter.addAction("android.net.wifi.WIFI_STATE_CHANGED");
    }

    public boolean isInitialized() {
        return mContext != null;
    }

    public void initialize(Context context) {
        mContext = context;
        mContext.registerReceiver(mConnectionReceiver, mFilter);
        ensureConnection(context);
    }

    public void deinitialize() {
        if (mContext != null) {
            mContext.unregisterReceiver(mConnectionReceiver);
            mContext = null;
        }
    }

    private final BroadcastReceiver mConnectionReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            ensureConnection(context);
        }
    };

    private void ensureConnection(Context context) {
        if (mContext == null)
            mContext = context;
        isNetworkWiFi = false;
        isNetworkMobile = false;
        ConnectivityManager connectivityManager = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivityManager != null) {
            NetworkInfo[] infoAvailableNetworks = connectivityManager.getAllNetworkInfo();
            if (infoAvailableNetworks != null) {
                for (NetworkInfo network : infoAvailableNetworks) {
                    if (network.getType() == ConnectivityManager.TYPE_WIFI) {
                        if (network.isConnected() && network.isAvailable())
                            isNetworkWiFi = true;
                    }
                    if (network.getType() == ConnectivityManager.TYPE_MOBILE) {
                        if (network.isConnected() && network.isAvailable())
                            isNetworkMobile = true;
                    }
                    notifyConnectionChange(isNetworkWiFi || isNetworkMobile);
                }
            }
        }
    }

    public boolean isConnected() {
        return isNetworkMobile || isNetworkWiFi;
    }

    public boolean isNetworkMobile() {
        return isNetworkMobile;
    }

    public boolean isNetworkWiFi() {
        return isNetworkWiFi;
    }

    public boolean canLoadGoodImage() {
        return PreferenceManager.getDefaultSharedPreferences(mContext).getBoolean("image_quality", true) ? isNetworkWiFi : true;
    }

    public boolean canSynchronize() {
        return PreferenceManager.getDefaultSharedPreferences(mContext).getBoolean("sync_wifi_only", false) ? isNetworkWiFi : true;
    }

    public String tryGetString(int id) {
        return mContext != null ? mContext.getString(id) : null;
    }

    public String getNetworkStateString(Context context) {
        if (isNetworkWiFi)
            return "Wi-Fi";
        else if (isNetworkMobile)
            return "3G";
        else
            return "No connection...";
    }

    public interface OnConnectionChangeListener {
        public void onConnectionChange(boolean isConnected);
    }

    public void attachOnConnectionChangeListener(OnConnectionChangeListener listener) {
        synchronized (mListeners) {
            mListeners.add(listener);
        }
    }

    public void detachOnConnectionChangeListener(OnConnectionChangeListener listener) {
        synchronized (mListeners) {
            mListeners.remove(listener);
        }
    }

    public void notifyConnectionChange(boolean isConnected) {
        synchronized (mListeners) {
            for (OnConnectionChangeListener listener : mListeners) {
                listener.onConnectionChange(isConnected);
            }
        }
    }

    public interface OnTrafficUpdateListener {
        void onTrafficUsageUpdate(long receive);
    }

    private Thread mTrafficMonitorThread;
    private ArrayList<OnTrafficUpdateListener> mTrafficListeners = new ArrayList<>();

    public void attachTrafficListener(OnTrafficUpdateListener listener) {
        synchronized (mTrafficListeners) {
            mTrafficListeners.add(listener);
        }
    }

    public void detachTrafficListener(OnTrafficUpdateListener listener) {
        synchronized (mTrafficListeners) {
            mTrafficListeners.remove(listener);
        }
    }

    private Handler trafficHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            synchronized (mTrafficListeners) {
                for (OnTrafficUpdateListener listener : mTrafficListeners) {
                    listener.onTrafficUsageUpdate((long) msg.obj);
                }
            }
        }
    };

    public void startTrafficMonitor() {
        if (mTrafficMonitorThread == null) {
            mTrafficMonitorThread = new Thread() {
                @Override
                public void run() {
                    super.run();
                    while (!isInterrupted()) {
                        long receive = TrafficStats.getUidRxBytes(mContext.getApplicationInfo().uid);
                        trafficHandler.obtainMessage(0, receive).sendToTarget();
                        try {
                            sleep(1000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }
            };
            mTrafficMonitorThread.start();
        }
    }

    public void stopTrafficMonitor() {
        if (mTrafficMonitorThread != null) {
            mTrafficMonitorThread.interrupt();
            mTrafficMonitorThread = null;
        }
    }

}
