package maestro.my.backgrounds;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.AsyncTaskLoader;
import android.support.v4.content.Loader;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.format.Formatter;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import maestro.my.backgrounds.data.Category;
import maestro.my.backgrounds.data.Parser;


public class Main extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Object>,
        NavigationView.OnNavigationItemSelectedListener, MHelper.OnTrafficUpdateListener {

    public static final String TAG = Main.class.getSimpleName();

    private NavigationView mNavigationView;
    private ActionBarDrawerToggle mDrawerToggle;
    private DrawerLayout mDrawer;
    private Toolbar mToolbar;
    private ProgressBar mProgress;
    private TextView mNetworkUsage;
    private FragmentTransaction mTransaction;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        mNavigationView = (NavigationView) findViewById(R.id.navigation_view);
        mDrawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mProgress = (ProgressBar) findViewById(R.id.progress);
        mNetworkUsage = (TextView) findViewById(R.id.network_usage_stats);
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawer, R.string.app_name, R.string.app_name) {
            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                commitTransaction();
            }
        };
        setSupportActionBar(mToolbar);
        mDrawer.setDrawerListener(mDrawerToggle);
        mNavigationView.setNavigationItemSelectedListener(this);
        getSupportLoaderManager().initLoader(TAG.hashCode(), null, this);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        MHelper.getInstance().attachTrafficListener(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        MHelper.getInstance().detachTrafficListener(this);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        int sortType = Parser.getSortType();
        for (int i = 0; i < menu.size(); i++) {
            MenuItem item = menu.getItem(i);
            item.setChecked(item.getItemId() == sortType);
        }
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {

        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            if (mDrawer.isDrawerOpen(Gravity.START)) {
                mDrawer.closeDrawers();
            } else {
                mDrawer.openDrawer(Gravity.START);
            }
            return true;
        } else if (id == R.id.sort_date || id == R.id.sort_downloads || id == R.id.sort_rating) {
            item.setChecked(true);
            changeSort(id);
            return true;
        } else if (id == R.id.search) {
            SearchFragment.makeInstance().show(getSupportFragmentManager(), SearchFragment.TAG);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void changeSort(int sort) {
        if (Parser.setSortType(sort)) {
            List<Fragment> fragmentList = getSupportFragmentManager().getFragments();
            if (fragmentList != null && fragmentList.size() > 0) {
                for (Fragment fragment : fragmentList) {
                    if (fragment instanceof LoadableFragment) {
                        ((LoadableFragment) fragment).reloadItems();
                    }
                }
            }
        }
    }

    @Override
    public void onBackPressed() {
        if (mDrawer.isDrawerOpen(Gravity.START)) {
            mDrawer.closeDrawers();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public Loader<Object> onCreateLoader(int id, Bundle args) {
        return new AsyncTaskLoader<Object>(this) {

            @Override
            protected void onStartLoading() {
                super.onStartLoading();
                forceLoad();
            }

            @Override
            public Object loadInBackground() {
                return Parser.getCategories();
            }
        };
    }

    @Override
    public void onLoadFinished(Loader<Object> loader, Object data) {
        if (data instanceof ArrayList) {
            ArrayList<Category> categories = (ArrayList<Category>) data;
            for (int i = 0; i < categories.size(); i++) {
                mNavigationView.getMenu().add(R.id.content_group,
                        categories.get(i).getId(), 100, categories.get(i).getTitle());
            }
            mNavigationView.getMenu().getItem(0).setChecked(true);
        } else {
            //TODO: display error
        }
        mProgress.animate().alpha(0).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                mProgress.setVisibility(View.GONE);
                openFragment(Parser.getFirstCategory().getId());
            }
        }).start();
    }

    @Override
    public void onLoaderReset(Loader<Object> loader) {

    }

    @Override
    public boolean onNavigationItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == R.id.settings) {
            startActivityForResult(new Intent(this, SettingsActivity.class), 0);
        } else {
            openFragment(menuItem.getItemId());
        }
        return true;
    }

    private void openFragment(int id) {
        mTransaction = getSupportFragmentManager().beginTransaction().replace(R.id.content_frame,
                ImagesFragment.makeInstance(id), "CategoryId:" + id);
        commitTransaction();
        mDrawer.closeDrawers();
        setTitle(Parser.getCategory(id).getTitle());
    }

    private void commitTransaction() {
        if (mTransaction != null && !mDrawer.isDrawerOpen(Gravity.START)) {
            mTransaction.commit();
            mTransaction = null;
        }
    }

    @Override
    public void onTrafficUsageUpdate(long receive) {
        mNetworkUsage.setText(Formatter.formatFileSize(this, receive));
    }
}
