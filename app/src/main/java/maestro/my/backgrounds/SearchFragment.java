package maestro.my.backgrounds;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.content.AsyncTaskLoader;
import android.support.v4.content.Loader;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import maestro.my.backgrounds.data.Parser;

/**
 * Created by Artyom on 9/9/2015.
 */
public class SearchFragment extends ImagesFragment {

    public static final String TAG = SearchFragment.class.getSimpleName();

    private static final String PARAM_QUERY = "query";

    public static final SearchFragment makeInstance() {
        SearchFragment fragment = new SearchFragment();
        Bundle args = new Bundle(1);
        args.putInt(PARAM_ID, Parser.SEARCH_CATEGORY_ID);
        fragment.setArguments(args);
        return fragment;
    }

    private EditText mSearchEdit;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View v = super.onCreateView(inflater, container, savedInstanceState);
        mSearchEdit = (EditText) v.findViewById(R.id.search_edit_text);
        mSearchEdit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String query = s.toString();
                Log.e(TAG, "query: " + query);
                if (TextUtils.isEmpty(query)) {
                    getCategory().getList().clear();
                    onUpdate(null, false);
                    getLoaderManager().destroyLoader(TAG.hashCode());
                } else {
                    Bundle bundle = new Bundle(1);
                    bundle.putString(PARAM_QUERY, query);
                    getLoaderManager().restartLoader(TAG.hashCode(), bundle, SearchFragment.this);
                }
            }
        });

        v.findViewById(R.id.btn_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        return v;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.search_fragment_view;
    }

    @Override
    public boolean isFullscreen() {
        return true;
    }

    @Override
    public Loader<Object> onCreateLoader(int id, Bundle args) {
        return args != null && args.containsKey(PARAM_QUERY) ? new SearchLoader(getActivity(), args.getString(PARAM_QUERY)) : super.onCreateLoader(id, args);
    }

    @Override
    public void onLoadFinished(Loader<Object> loader, Object data) {
        getCategory().getList().clear();
        Log.e(TAG, "onLoadFinished: " + data);
        super.onLoadFinished(loader, data);
    }

    public static final class SearchLoader extends AsyncTaskLoader<Object> {

        private String mQuery;

        public SearchLoader(Context context, String query) {
            super(context);
            mQuery = query;
        }

        @Override
        protected void onStartLoading() {
            super.onStartLoading();
            forceLoad();
        }

        @Override
        public Object loadInBackground() {
            return Parser.search(mQuery);
        }
    }

}
