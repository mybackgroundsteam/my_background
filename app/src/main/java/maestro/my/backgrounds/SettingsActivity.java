package maestro.my.backgrounds;

import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceActivity;

public class SettingsActivity extends PreferenceActivity implements Preference.OnPreferenceChangeListener {

    public static final String CHANGE_QUALITY_WIFI = "change_quality_on_wifi";
    public static final String IMAGE_QUALITY_WIFI = "image_quality_wifi";
    public static final String IMAGE_QUALITY_3G = "image_quality_3g";
    public static final String PREVIEW_IMAGE_QUALITY_WIFI = "preview_image_quality_wifi";
    public static final String PREVIEW_IMAGE_QUALITY_3G = "preview_image_quality_3g";

    private String ofImageStr;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preferences);
        ofImageStr = getString(R.string.of_image);
        updateImageQualityPreferenceSummery(IMAGE_QUALITY_WIFI, "100%");
        updateImageQualityPreferenceSummery(IMAGE_QUALITY_3G, "60%");
        updateImageQualityPreferenceSummery(PREVIEW_IMAGE_QUALITY_WIFI, "80%");
        updateImageQualityPreferenceSummery(PREVIEW_IMAGE_QUALITY_3G, "50%");
    }

    private void updateImageQualityPreferenceSummery(String key, String defValue) {
        Preference preference = findPreference(key);
        preference.setSummary(String.format(ofImageStr,
                preference.getSharedPreferences().getString(key, defValue) + "%"));
        preference.setOnPreferenceChangeListener(this);
    }

    @Override
    public boolean onPreferenceChange(Preference preference, Object newValue) {
        if (preference.getKey().equals(IMAGE_QUALITY_WIFI)
                || preference.getKey().equals(IMAGE_QUALITY_3G)
                || preference.getKey().equals(PREVIEW_IMAGE_QUALITY_WIFI)
                || preference.getKey().equals(PREVIEW_IMAGE_QUALITY_3G)) {
            preference.setSummary(String.format(ofImageStr, newValue + "%"));
            return true;
        }
        return false;
    }
}
