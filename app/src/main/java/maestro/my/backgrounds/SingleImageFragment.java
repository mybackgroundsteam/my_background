package maestro.my.backgrounds;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.webkit.MimeTypeMap;
import android.widget.ProgressBar;

import com.dream.android.mim.ImageLoadObject;
import com.dream.android.mim.MIM;
import com.dream.android.mim.MIMUtils;
import com.dream.android.mim.OnPreLoadListener;
import com.dream.android.mim.RecyclingImageView;

import maestro.my.backgrounds.data.DownloadManager;
import maestro.my.backgrounds.data.Image;
import maestro.my.backgrounds.data.Parser;
import maestro.my.backgrounds.data.Size;

/**
 * Created by Artyom on 8/30/2015.
 */
public final class SingleImageFragment extends Fragment implements OnClickListener {

    public static final String TAG = SingleImageFragment.class.getSimpleName();

    private static final String PARAM_IMAGE = "image";

    public static final SingleImageFragment makeInstance(Image image) {
        SingleImageFragment fragment = new SingleImageFragment();
        Bundle args = new Bundle(1);
        args.putParcelable(PARAM_IMAGE, image);
        fragment.setArguments(args);
        return fragment;
    }

    private Image mImage;
    private RecyclingImageView mImageView;
    private ProgressBar mProgress;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View v = inflater.inflate(R.layout.single_image_view, null);
        mImageView = (RecyclingImageView) v.findViewById(R.id.image);
        mProgress = (ProgressBar) v.findViewById(R.id.progress);
        v.findViewById(R.id.set_as_background).setOnClickListener(this);
        v.findViewById(R.id.share).setOnClickListener(this);
        return v;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mImage = getArguments().getParcelable(PARAM_IMAGE);
        mImageView.setLoadObject(MIM.by(BApplication.MIM_INTERNET).to(mImageView, MIMUtils.makeOKey(getActivity(), mImage.getThumbUrl(), "preview"), mImage.getThumbUrl())
                .preLoadListener(new OnPreLoadListener() {
                    @Override
                    public void onPreLoad(ImageLoadObject imageLoadObject) {
                        Size size = Parser.adjustSize(mImageView.getMeasuredWidth(), mImageView.getMeasuredHeight(), true);
                        if (size != null) {
                            imageLoadObject.path(mImage.getAlternateUrl(mImage.getThumbUrl(), size));
                        }
                        Log.e(TAG, "size: " + size);
                    }
                })
                .listener(new ImageLoadObject.OnImageLoadEventListener() {
                    @Override
                    public void onImageLoadEvent(IMAGE_LOAD_EVENT image_load_event, ImageLoadObject imageLoadObject) {
                        if (image_load_event == IMAGE_LOAD_EVENT.FINISH) {
                            mProgress.animate().alpha(0).setListener(new AnimatorListenerAdapter() {
                                @Override
                                public void onAnimationEnd(Animator animation) {
                                    super.onAnimationEnd(animation);
                                    mProgress.setVisibility(View.GONE);
                                }
                            }).start();
                        }
                    }
                }));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.set_as_background:
                DownloadManager.getInstance().download(mImage, true);
                break;
            case R.id.share:
                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.setData(Uri.parse(mImage.getAlternateUrl(mImage.getThumbUrl(), Parser.getSize(1920, 1080))));
                intent.setType("image/*");
                if (intent.resolveActivity(getActivity().getPackageManager()) != null) {
                    getActivity().startActivity(intent);
                } else {
                    //TODO: add error
                }
                break;
        }
    }
}
