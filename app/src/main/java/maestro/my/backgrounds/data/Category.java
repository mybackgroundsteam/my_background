package maestro.my.backgrounds.data;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Artyom on 8/29/2015.
 */
public class Category implements Parcelable {

    private int mId;
    private String mTitle;
    private String mUrl;
    private ImageList mList;

    public Category(int id, String title, String url) {
        mId = id;
        mTitle = title;
        mUrl = url;
        mList = new ImageList(mUrl);
    }

    public Category(Parcel source) {
        mId = source.readInt();
        mTitle = source.readString();
        mUrl = source.readString();
        mList = source.readParcelable(ImageList.class.getClassLoader());
    }

    public int getId() {
        return mId;
    }

    public ImageList getList() {
        return mList;
    }

    public String getTitle() {
        return mTitle;
    }

    public String getUrl() {
        return mUrl;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(mId);
        dest.writeString(mTitle);
        dest.writeString(mUrl);
        dest.writeParcelable(mList, 0);
    }

    public static final Creator<Category> CREATOR = new Creator<Category>() {
        @Override
        public Category createFromParcel(Parcel source) {
            return new Category(source);
        }

        @Override
        public Category[] newArray(int size) {
            return new Category[size];
        }
    };

    public void updateUrl(String url) {
        mUrl = url;
        mList.setUrl(url);
    }
}
