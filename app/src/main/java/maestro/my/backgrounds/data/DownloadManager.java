package maestro.my.backgrounds.data;

import android.app.Application;
import android.app.NotificationManager;
import android.app.WallpaperManager;
import android.content.Context;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.v4.app.NotificationCompat;
import android.util.DisplayMetrics;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.net.ssl.HttpsURLConnection;

import maestro.my.backgrounds.BApplication;
import maestro.my.backgrounds.R;

/**
 * Created by Artyom on 9/15/2015.
 */
public class DownloadManager {

    private static volatile DownloadManager instance;
    private static final int MSG_DOWNLOAD_START = 0;
    private static final int MSG_DOWNLOAD_PROGRESS = 1;
    private static final int MSG_DOWNLOAD_FINISH = 2;
    private static final String PARAM_IMAGE = "image";

    public static synchronized DownloadManager getInstance() {
        return instance != null ? instance : (instance = new DownloadManager());
    }

    DownloadManager() {
    }

    private Application mApplication;
    private ArrayList<OnDownloadListener> mListeners = new ArrayList<>();
    private NotificationManager mNotificationManager;
    private NotificationCompat.Builder mNotificationBuilder;

    public void initialize(BApplication application) {
        mApplication = application;
        mNotificationManager = (NotificationManager) mApplication.getSystemService(Context.NOTIFICATION_SERVICE);
    }

    private ArrayList<Image> mQueue = new ArrayList<>(1);
    private ExecutorService mExecutor = Executors.newSingleThreadExecutor();

    private final Handler uiHandler = new Handler(Looper.getMainLooper()) {
        @Override
        public void handleMessage(Message msg) {
            Image image = msg.getData().getParcelable(PARAM_IMAGE);
            switch (msg.what) {
                case MSG_DOWNLOAD_START:
                    synchronized (mListeners) {
                        for (OnDownloadListener listener : mListeners) {
                            listener.onDownloadStart(image);
                        }
                    }
                    break;
                case MSG_DOWNLOAD_PROGRESS:
                    synchronized (mListeners) {
                        for (OnDownloadListener listener : mListeners) {
                            listener.onDownloadProgress(image, (Integer) msg.obj);
                        }
                    }
                    break;
                case MSG_DOWNLOAD_FINISH:
                    synchronized (mListeners) {
                        for (OnDownloadListener listener : mListeners) {
                            listener.onDownloadFinish(image, image);
                        }
                    }
                    break;
            }
        }
    };

    public void download(Image image, boolean setAsBackground) {
        synchronized (mQueue) {
            if (mQueue.contains(image)) {
                //TODO: swap listeners?
            } else {
                mQueue.add(image);
                mExecutor.execute(new DownloadTask(image, setAsBackground));
            }
        }
    }

    public boolean contains(Image image) {
        synchronized (mQueue) {
            return mQueue.contains(image);
        }
    }

    private class DownloadTask implements Runnable {

        private Image mImage;
        private boolean setAsBackground;

        public DownloadTask(Image image, boolean asBackground) {
            mImage = image;
            setAsBackground = asBackground;
        }

        @Override
        public void run() {
            notifyEvent(mImage, MSG_DOWNLOAD_START, null);
            File file = Environment.getExternalStorageDirectory();
            if (file == null || !file.canWrite()) {
                file = Environment.getDownloadCacheDirectory();
            }
            File mDownloadFile = null;
            Object error = null;
            if (file == null || !file.canWrite()) {
                error = file;
            } else {
                DisplayMetrics metrics = mApplication.getResources().getDisplayMetrics();
                try {
                    String url = Image.getAlternateUrl(mImage.getThumbUrl(), Parser.getSize(metrics.widthPixels, metrics.heightPixels));
                    HttpsURLConnection connection = Parser.getConnection(url);
                    connection.connect();
                    int length = connection.getContentLength();
                    int count = 0;
                    InputStream input = new BufferedInputStream(connection.getInputStream(), 8192);
                    mDownloadFile = new File(file + "/My backgrounds", String.valueOf(url.hashCode()));
                    mDownloadFile.mkdirs();
                    if (mDownloadFile.exists()) {
                        mDownloadFile.delete();
                    } else {
                        mDownloadFile.createNewFile();
                    }
                    OutputStream output = new FileOutputStream(mDownloadFile);
                    byte data[] = new byte[1024];
                    long total = 0;
                    int progress = 0;
                    int prevProgress = 0;
                    long prevTime = 0;
                    while ((count = input.read(data)) != -1) {
                        total += count;
                        progress = (int) ((total * 100) / length);
                        if (prevProgress != progress && (System.currentTimeMillis() - prevTime) > 500) {
                            notifyEvent(mImage, MSG_DOWNLOAD_PROGRESS, progress);
                            prevProgress = progress;
                            prevTime = System.currentTimeMillis();
                        }
                        output.write(data, 0, count);
                    }
                    output.flush();
                    output.close();
                    input.close();

                    if (setAsBackground && mDownloadFile != null) {
                        WallpaperManager manager = WallpaperManager.getInstance(mApplication.getApplicationContext());
                        try {
                            manager.setStream(new FileInputStream(mDownloadFile));
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                    error = e;
                } catch (UnrecoverableKeyException e) {
                    e.printStackTrace();
                    error = e;
                } catch (NoSuchAlgorithmException e) {
                    e.printStackTrace();
                    error = e;
                } catch (KeyStoreException e) {
                    e.printStackTrace();
                    error = e;
                } catch (KeyManagementException e) {
                    e.printStackTrace();
                    error = e;
                }
            }
            if (error != null) {
                mDownloadFile.delete();
            }
            notifyEvent(mImage, MSG_DOWNLOAD_FINISH, error);
            synchronized (mQueue) {
                mQueue.remove(mImage);
            }
        }
    }

    public void addListener(OnDownloadListener listener) {
        synchronized (mListeners) {
            mListeners.add(listener);
        }
    }

    public void removeListener(OnDownloadListener listener) {
        synchronized (mListeners) {
            mListeners.remove(listener);
        }
    }

    private void notifyEvent(Image image, int code, Object object) {
        Message msg = Message.obtain(uiHandler, code, object);
        msg.getData().putParcelable(PARAM_IMAGE, image);
        msg.sendToTarget();
        if (mNotificationBuilder == null) {
            mNotificationBuilder = new NotificationCompat.Builder(mApplication).setContentTitle(mApplication.getString(R.string.downloading))
                    .setSmallIcon(R.mipmap.ic_launcher);
        }
        mNotificationBuilder.setContentText(image.getTitle()).build();
        switch (code) {
            case MSG_DOWNLOAD_FINISH:
                mNotificationManager.cancel(0);
                return;
            case MSG_DOWNLOAD_PROGRESS:
                int progress = (Integer) object;
                mNotificationBuilder.setProgress(100, progress, progress == 0);
            case MSG_DOWNLOAD_START:
                mNotificationManager.notify(0, mNotificationBuilder.build());
                break;
        }
    }

    public interface OnDownloadListener {
        void onDownloadStart(Image image);

        void onDownloadProgress(Image image, int progress);

        void onDownloadFinish(Image image, Object error);
    }

}
