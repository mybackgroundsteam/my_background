package maestro.my.backgrounds.data;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

/**
 * Created by Artyom on 8/29/2015.
 */
public class Image implements Parcelable {

    private String mTitle;
    private String mUrl;
    private String mThumbUrl;
    private String mRating;
    private String mDownloadCount;
    private String mResolution;

    public Image() {
    }

    public Image(String title, String url, String thumbUrl, String rating, String downloadCount, String resolution) {
        mTitle = title;
        mUrl = url;
        mThumbUrl = thumbUrl;
        mRating = rating;
        mDownloadCount = downloadCount;
        mResolution = resolution;
    }

    public Image(Parcel source) {
        mTitle = source.readString();
        mUrl = source.readString();
        mThumbUrl = source.readString();
        mRating = source.readString();
        mDownloadCount = source.readString();
        mResolution = source.readString();
    }

    public String getTitle() {
        return mTitle;
    }

    public String getUrl() {
        return mUrl;
    }

    public String getThumbUrl() {
        return mThumbUrl;
    }

    public String getRating() {
        return mRating;
    }

    public String getDownloadCount() {
        return mDownloadCount;
    }

    public String getResolution() {
        return mResolution;
    }

    public void setTitle(String mTitle) {
        this.mTitle = mTitle;
    }

    public void setUrl(String mUrl) {
        this.mUrl = mUrl;
    }

    public void setThumbUrl(String mThumbUrl) {
        this.mThumbUrl = mThumbUrl;
    }

    public void setRating(String mRating) {
        this.mRating = mRating;
    }

    public void setDownloadCount(String mDownloadCount) {
        this.mDownloadCount = mDownloadCount;
    }

    public void setResolution(String mResolution) {
        this.mResolution = mResolution;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mTitle);
        dest.writeString(mUrl);
        dest.writeString(mThumbUrl);
        dest.writeString(mRating);
        dest.writeString(mDownloadCount);
        dest.writeString(mResolution);
    }

    @Override
    public boolean equals(Object o) {
        return super.equals(o) || (o instanceof Image && ((Image) o).getUrl().equals(getUrl()));
    }

    public static final Creator<Image> CREATOR = new Creator<Image>() {
        @Override
        public Image createFromParcel(Parcel source) {
            return new Image(source);
        }

        @Override
        public Image[] newArray(int size) {
            return new Image[size];
        }
    };

    public static String getAlternateUrl(String mThumbUrl, Size size) {
        if (!TextUtils.isEmpty(mThumbUrl)) {
            int lastIndex = mThumbUrl.lastIndexOf("_");
            int lastIndexOfDot = mThumbUrl.lastIndexOf(".");
            return new StringBuilder().append(mThumbUrl.substring(0, lastIndex + 1))
                    .append(size.getWidth()).append("x").append(size.getHeight())
                    .append(mThumbUrl.substring(lastIndexOfDot)).toString();
        }
        return null;
    }

}