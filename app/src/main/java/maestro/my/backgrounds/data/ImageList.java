package maestro.my.backgrounds.data;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

import java.util.Arrays;

/**
 * Created by Artyom on 8/29/2015.
 */
public class ImageList implements Parcelable {

    private static final String mPattern = "%s/page%d";
    private static final String mSortPattern = "%s/" + mPattern;
    private static final String mSortPatternShort = "%s/%s";

    private Image[] mImages = new Image[0];
    private String mUrl;
    private int mCurrentPage = 0;
    private int mMaxPage;
    private int mSize;

    public ImageList(String url) {
        mUrl = url;
    }

    public ImageList(Parcel source) {
        mImages = (Image[]) source.readParcelableArray(Image.class.getClassLoader());
        mUrl = source.readString();
        mCurrentPage = source.readInt();
        mMaxPage = source.readInt();
        mSize = source.readInt();
    }

    public void setPages(int current, int max) {
        mCurrentPage = current;
        mMaxPage = max;
    }

    public int getCurrentPage() {
        return mCurrentPage;
    }

    public int getMaxPage() {
        return mMaxPage;
    }

    public String getUrl() {
        return mUrl;
    }

    public boolean hasNext() {
        return mCurrentPage == 0 || mCurrentPage != mMaxPage;
    }

    public int getCount() {
        return mSize;
    }

    public Image getImageAtPosition(int position) {
        return mImages[position];
    }

    public Image[] getImages() {
        return mImages;
    }

    public void addImage(Image image) {
        Image[] a = mImages;
        int s = mSize;
        if (s == a.length) {
            Image[] newArray = new Image[s + 1];
            System.arraycopy(a, 0, newArray, 0, s);
            mImages = a = newArray;
        }
        a[s] = image;
        mSize = s + 1;
    }

    public void addImages(Image[] images) {
        int newPartSize = images.length;
        if (newPartSize == 0) {
            return;
        }
        Image[] a = mImages;
        int s = mSize;
        int newSize = s + newPartSize;
        if (newSize > a.length) {
            Image[] newArray = new Image[newSize];
            System.arraycopy(a, 0, newArray, 0, s);
            mImages = a = newArray;
        }
        System.arraycopy(images, 0, a, s, newPartSize);
        mSize = newSize;
    }

    public void clear() {
        mCurrentPage = 0;
        Arrays.fill(mImages, 0, mSize, null);
        mImages = new Image[0];
        mSize = 0;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

    }

    public static final Creator<ImageList> CREATOR = new Creator<ImageList>() {
        @Override
        public ImageList createFromParcel(Parcel source) {
            return new ImageList(source);
        }

        @Override
        public ImageList[] newArray(int size) {
            return new ImageList[size];
        }
    };

    public String getLoadUrl(String sortParam) {
        if (mUrl.equals("https://wallpaperscraft.com/all")) {
            if (sortParam.equals(Parser.PARAM_DATE)) {
                sortParam = null;
            }
        } else if (sortParam.equals(Parser.PARAM_RATING)) {
            sortParam = null;
        }
        if (mCurrentPage == 0) {
            return !TextUtils.isEmpty(sortParam) ? String.format(mSortPatternShort, mUrl, sortParam) : mUrl;
        } else if (!TextUtils.isEmpty(sortParam)) {
            return String.format(mSortPattern, mUrl, sortParam, mCurrentPage + 1);
        }
        return String.format(mPattern, mUrl, mCurrentPage + 1);
    }

    public void setUrl(String url) {
        clear();
        mUrl = url;
    }
}
