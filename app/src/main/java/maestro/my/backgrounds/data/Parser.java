package maestro.my.backgrounds.data;

import android.app.Application;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.util.Log;

import org.htmlcleaner.HtmlCleaner;
import org.htmlcleaner.TagNode;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;

import maestro.my.backgrounds.MHelper;
import maestro.my.backgrounds.R;
import maestro.my.backgrounds.SettingsActivity;

/**
 * Created by Artyom on 8/29/2015.
 */
public class Parser {

    public static final String TAG = Parser.class.getSimpleName();

    public static final int SEARCH_CATEGORY_ID = -3;

    private static final String CategoryUrl = "https://wallpaperscraft.com/";
    private static final String SearchUrl = "https://wallpaperscraft.com/search/keywords?q=%s";
    private static final String PARAM_SORT_TYPE = "sort_type";

    public static final String PARAM_DATE = "date";
    public static final String PARAM_RATING = "ratings";
    public static final String PARAM_DOWNLOADS = "downloads";

    private static final Category mSearchCategory = new Category(SEARCH_CATEGORY_ID, null, null);
    private static final ArrayList<Category> mCategoryCache = new ArrayList<>();
    private static final ArrayList<Size> mSizes = new ArrayList<>();
    private static final HashMap<String, Size> mSizesCache = new HashMap<>();

    public enum ERROR {
        UNKNOWN, CONNECTION, SERVER
    }

    private static Application mApplication;
    private static SharedPreferences mPreferences;

    public static final void initialize(Application application) {
        mApplication = application;
        mPreferences = PreferenceManager.getDefaultSharedPreferences(application);
    }

    public static final int getSortType() {
        return mPreferences.getInt(PARAM_SORT_TYPE, R.id.sort_date);
    }

    public static final String getSortTypeStr() {
        final int sortType = getSortType();
        return sortType == R.id.sort_rating ? PARAM_RATING : sortType == R.id.sort_downloads ? PARAM_DOWNLOADS : PARAM_DATE;
    }

    public static boolean setSortType(int sortType) {
        if (sortType != getSortType()) {
            mPreferences.edit().putInt(PARAM_SORT_TYPE, sortType).apply();
            synchronized (mCategoryCache) {
                for (int i = 0; i < mCategoryCache.size(); i++) {
                    mCategoryCache.get(i).getList().clear();
                }
            }
            return true;
        }
        return false;
    }

    public static Object getCategories() {
        if (mCategoryCache.size() > 0) {
            return mCategoryCache;
        }
        try {
            TagNode rootNode = new HtmlCleaner().clean(getStream(CategoryUrl));
            TagNode categoryNode = rootNode.findElementByAttValue("class", "left_category", true, false);
            ArrayList<Category> mCategories = new ArrayList<>();
            for (int i = 0; i < categoryNode.getChildren().size(); i++) {
                TagNode child = categoryNode.getChildren().get(i);
                if (child.getChildren().size() == 0) {
                    continue;
                }
                child = child.getChildren().get(0);
                String attr = child.getAttributeByName("class");
                if (TextUtils.isEmpty(attr)) {
                    continue;
                }
                if (attr.equals("menu_select")) {
                    String url = fixUrl(child.getAttributeByName("href"));
                    mCategories.add(new Category(i, String.valueOf(child.getText()), url));
                } else if (attr.equals("menu_active")) {
                    mCategories.add(new Category(i, String.valueOf(child.getText()), CategoryUrl + "all"));
                }
            }
            synchronized (mCategoryCache) {
                mCategoryCache.addAll(mCategories);
            }
            return mCategoryCache;
        } catch (Exception e) {
            e.printStackTrace();
            return ERROR.CONNECTION;
        }
    }

    public static Object search(String query) {
        mSearchCategory.updateUrl(String.format(SearchUrl, query));
        return loadNextImages(mSearchCategory.getList());
    }

    public static Object loadNextImages(ImageList list) {
        try {
            String sort = getSortTypeStr();
            TagNode rootNode = new HtmlCleaner().clean(getStream(list.getLoadUrl(sort)));
            Log.e(TAG, "loadImages: " + list.getLoadUrl(sort));
            TagNode pageNode = rootNode.findElementByAttValue("class", "pages", true, false);
            TagNode last = pageNode.getChildren().get(pageNode.getChildren().size() - 1);
            int current = list.getCurrentPage() + 1;
            int max = last.getAttributeByName("href").equals("#") ? list.getMaxPage() : Integer.valueOf(last.getText().toString());
            Log.e(TAG, "current: " + current + ", max: " + max);
            list.setPages(current, max);

            TagNode categoryNode = rootNode.findElementByAttValue("class", "wallpapers", true, false);
            for (int i = 0; i < categoryNode.getChildren().size(); i++) {
                TagNode node = categoryNode.getChildren().get(i);
                TagNode a = node.findElementByName("a", true);
                Image image = new Image();
                image.setUrl(fixUrl(a.getAttributeByName("href")));
                a = node.findElementByName("img", true);
                image.setThumbUrl(fixUrl(a.getAttributeByName("src")));
                a = node.findElementByAttValue("class", "pre_name", true, false);
                image.setTitle(String.valueOf(a.getText()));
                node = node.findElementByAttValue("class", "pre_info", true, false);
                a = findNodeContains(node, "pre_rate");
                if (a != null)
                    image.setRating(String.valueOf(a.getText()));
                a = node.findElementByAttValue("class", "pre_count", true, false);
                if (a != null)
                    image.setDownloadCount(String.valueOf(a.getText()));
                a = node.findElementByAttValue("class", "pre_size", true, false);
                if (a != null)
                    image.setResolution(String.valueOf(a.getText()));
                list.addImage(image);
            }
            return null;
        } catch (Exception e) {
            e.printStackTrace();
            return ERROR.CONNECTION;
        }
    }

    private static TagNode findNodeContains(TagNode parent, String attr) {
        for (int i = 0; i < parent.getChildren().size(); i++) {
            Map<String, String> map = parent.getChildren().get(i).getAttributes();
            for (String string : map.keySet()) {
                if (string.contains(attr)) {
                    return parent.getChildren().get(i);
                }
            }
        }
        return null;
    }

    private static final String fixUrl(String url) {
        if (url.startsWith("//")) {
            url = "https://" + url.substring(2);
        }
        return url;
    }

    public static Category getFirstCategory() {
        synchronized (mCategoryCache) {
            return mCategoryCache.size() > 0 ? mCategoryCache.get(0) : null;
        }
    }

    public static Category getCategory(int id) {
        if (id == SEARCH_CATEGORY_ID) {
            return mSearchCategory;
        }
        synchronized (mCategoryCache) {
            for (int i = 0; i < mCategoryCache.size(); i++) {
                if (mCategoryCache.get(i).getId() == id) {
                    return mCategoryCache.get(i);
                }
            }
            return null;
        }
    }

    public static Size getSize(int width, int height) {
        Log.e(TAG, "getSize: " + width + "/" + height);
        final String key = new StringBuilder().append(width).append(height).toString();
        Size sDiff = mSizesCache.get(key);
        if (sDiff != null) {
            Log.e(TAG, "cache: " + sDiff);
            return sDiff;
        }
        sDiff = new Size(0, 0);
        int adjWidth = width;
        int adjDiff = -1;
        for (int i = 0; i < mSizes.size(); i++) {
            Size size = mSizes.get(i);
            int diff = size.getWidth() - width;
            if (adjDiff == -1 || (diff >= 0 && adjDiff > diff)) {
                adjDiff = diff;
                adjWidth = size.getWidth();
            }
        }

        int adjHeight = height;
        adjDiff = -1;
        for (int i = 0; i < mSizes.size(); i++) {
            Size size = mSizes.get(i);
            int diff = size.getHeight() - height;
            if (diff >= 0 && (adjDiff == -1 || adjDiff > diff)) {
                adjDiff = diff;
                adjHeight = size.getHeight();
            }
        }
        width = adjWidth;
        height = adjHeight;
        boolean byWidth = width > height;
        Log.e(TAG, "adjWidth: " + adjWidth + ", adjHeight: " + adjHeight);
        for (int i = 0; i < mSizes.size(); i++) {
            Size size = mSizes.get(i);
            if (size.getWidth() == width && size.getHeight() == height) {
                return size;
            }
            if (sDiff == null) {
                sDiff = size;
            } else if (byWidth && size.getWidth() == adjWidth
                    && size.getHeight() > sDiff.getHeight() && size.getHeight() <= adjHeight) {
                sDiff = size;
            } else if (!byWidth && size.getHeight() == adjHeight
                    && size.getHeight() > sDiff.getHeight() && size.getHeight() <= adjHeight) {
                sDiff = size;
            }
        }
        mSizesCache.put(key, sDiff);
        return sDiff;
    }

    public static final InputStream getStream(String url) throws IOException, UnrecoverableKeyException, NoSuchAlgorithmException, KeyStoreException, KeyManagementException {
        HttpsURLConnection connection = (HttpsURLConnection) new URL(url).openConnection();
        connection.setSSLSocketFactory(TrustAllSSLSocketFactory.getInstance());
        return connection.getInputStream();
    }

    public static final HttpsURLConnection getConnection(String url) throws IOException, UnrecoverableKeyException, NoSuchAlgorithmException, KeyStoreException, KeyManagementException {
        HttpsURLConnection connection = (HttpsURLConnection) new URL(url).openConnection();
        connection.setSSLSocketFactory(TrustAllSSLSocketFactory.getInstance());
        return connection;
    }

    public static final Size adjustSize(float width, float height, boolean fullSize) {
        float imageQuality;
        if (MHelper.getInstance().isNetworkWiFi()
                || !PreferenceManager.getDefaultSharedPreferences(mApplication).getBoolean(SettingsActivity.CHANGE_QUALITY_WIFI, true)) {
            imageQuality = getFloat(mPreferences.getString(fullSize
                            ? SettingsActivity.IMAGE_QUALITY_WIFI : SettingsActivity.PREVIEW_IMAGE_QUALITY_WIFI,
                    fullSize ? "80%" : "100%"));
        } else {
            imageQuality = getFloat(mPreferences.getString(fullSize
                            ? SettingsActivity.IMAGE_QUALITY_3G : SettingsActivity.PREVIEW_IMAGE_QUALITY_3G,
                    fullSize ? "50%" : "60%"));
        }
        return getSize((int) (width * imageQuality), (int) (height * imageQuality));
    }

    private static float getFloat(String percentString) {
        return Float.valueOf(percentString.substring(0, percentString.length() - 1)) / 100f;
    }

    static {
        mSizes.add(new Size(1600, 1200));
        mSizes.add(new Size(1400, 1050));
        mSizes.add(new Size(1280, 1024));
        mSizes.add(new Size(1260, 960));
        mSizes.add(new Size(1152, 864));
        mSizes.add(new Size(1024, 768));
        mSizes.add(new Size(3840, 2400));
        mSizes.add(new Size(3840, 2160));
        mSizes.add(new Size(3840, 1200));
        mSizes.add(new Size(2560, 1600));
        mSizes.add(new Size(2560, 1440));
        mSizes.add(new Size(2560, 1080));
        mSizes.add(new Size(2560, 1024));
        mSizes.add(new Size(2048, 1152));
        mSizes.add(new Size(1920, 1200));
        mSizes.add(new Size(1920, 1080));
        mSizes.add(new Size(1680, 1050));
        mSizes.add(new Size(1600, 900));
        mSizes.add(new Size(1440, 900));
        mSizes.add(new Size(1280, 800));
        mSizes.add(new Size(1280, 720));
        mSizes.add(new Size(1366, 768));
        mSizes.add(new Size(1080, 1920));
        mSizes.add(new Size(1024, 600));
        mSizes.add(new Size(960, 544));
        mSizes.add(new Size(800, 1280));
        mSizes.add(new Size(800, 600));
        mSizes.add(new Size(720, 1280));
        mSizes.add(new Size(540, 960));
        mSizes.add(new Size(480, 854));
        mSizes.add(new Size(480, 800));
        mSizes.add(new Size(400, 480));
        mSizes.add(new Size(360, 640));
        mSizes.add(new Size(320, 480));
        mSizes.add(new Size(320, 240));
        mSizes.add(new Size(240, 400));
        mSizes.add(new Size(240, 320));
        mSizes.add(new Size(2048, 2048));
        mSizes.add(new Size(1024, 1024));
        mSizes.add(new Size(750, 1334));
        mSizes.add(new Size(640, 1136));
        mSizes.add(new Size(640, 960));
    }

}
