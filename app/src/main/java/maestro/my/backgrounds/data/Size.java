package maestro.my.backgrounds.data;

/**
 * Created by Artyom on 8/31/2015.
 */
public class Size {

    private int mWidth;
    private int mHeight;

    public Size(int width, int height) {
        mWidth = width;
        mHeight = height;
    }

    public int getWidth() {
        return mWidth;
    }

    public int getHeight() {
        return mHeight;
    }

    public int getDiffW(int width) {
        return mWidth = width;
    }

    public int getDiffH(int height) {
        return mHeight;
    }

    @Override
    public String toString() {
        return new StringBuilder()
                .append(getClass().getSimpleName())
                .append("@")
                .append(Integer.toHexString(hashCode()))
                .append(": w = ")
                .append(mWidth)
                .append(", h = ")
                .append(mHeight)
                .toString();
    }
}
