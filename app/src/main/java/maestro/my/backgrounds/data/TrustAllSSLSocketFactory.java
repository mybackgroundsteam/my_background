package maestro.my.backgrounds.data;

import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

/**
 * This SSL socket factory is used to accept all server SSL certificates, even those that
 * are not trusted.
 */
public class TrustAllSSLSocketFactory extends SSLSocketFactory {
    private static TrustAllSSLSocketFactory instance;

    private javax.net.ssl.SSLSocketFactory factory;

    /**
     * Get instance of factory.
     *
     * @return
     * @throws KeyManagementException
     * @throws NoSuchAlgorithmException
     * @throws KeyStoreException
     * @throws UnrecoverableKeyException
     */
    public static TrustAllSSLSocketFactory getInstance() throws KeyManagementException,
            NoSuchAlgorithmException, KeyStoreException, UnrecoverableKeyException {
        if (instance == null) {
            instance = new TrustAllSSLSocketFactory();
        }
        return instance;
    }

    private TrustAllSSLSocketFactory() throws KeyManagementException,
            NoSuchAlgorithmException, KeyStoreException,
            UnrecoverableKeyException {
        super();
        try {
            SSLContext sslcontext = SSLContext.getInstance("TLS");
            X509TrustManager trustMgr = new X509TrustManager() {
                public X509Certificate[] getAcceptedIssuers() {
                    return null;
                }

                public void checkServerTrusted(X509Certificate[] chain,
                                               String authType) throws CertificateException {
                }

                public void checkClientTrusted(X509Certificate[] chain,
                                               String authType) throws CertificateException {
                }
            };
            sslcontext.init(null,
                    new TrustManager[]{trustMgr}, null);
            factory = sslcontext.getSocketFactory();
        } catch (Exception ex) {
        }
    }

    @Override
    public Socket createSocket() throws IOException {
        return factory.createSocket();
    }

    @Override
    public Socket createSocket(Socket socket, String s, int i, boolean flag)
            throws IOException {
        return factory.createSocket(socket, s, i, flag);
    }

    /**
     * Delegate method for {@link  javax.net.SocketFactory#createSocket(InetAddress address, int port, InetAddress localAddress, int localPort) createSocket(address, port, localAddress, localPort)
     *
     * @param address
     * @param port
     * @param localAddress
     * @param localPort
     * @return
     * @throws IOException
     */
    public Socket createSocket(InetAddress address, int port,
                               InetAddress localAddress, int localPort) throws IOException {
        return factory.createSocket(address, port, localAddress, localPort);
    }

    /**
     * Delegate method for {@link  javax.net.SocketFactory#createSocket(InetAddress host, int port) createSocket(host, port)
     *
     * @param host
     * @param port
     * @return
     * @throws IOException
     */
    public Socket createSocket(InetAddress host, int port)
            throws IOException {
        return factory.createSocket(host, port);
    }

    /**
     * Delegate method for {@link  javax.net.SocketFactory#createSocket(String host, int port, InetAddress localHost, int localPort) createSocket(String host, int port, InetAddress localHost, int localPort)
     *
     * @param host
     * @param port
     * @param inaddr
     * @param localPort
     * @return
     * @throws IOException
     */
    public Socket createSocket(String host, int port, InetAddress inaddr, int localPort)
            throws IOException {
        return factory.createSocket(host, port, inaddr, localPort);
    }

    /**
     * Delegate method for {@link  javax.net.SocketFactory#createSocket(String host, int port) createSocket(String host, int port)
     *
     * @param host
     * @param port
     * @return
     * @throws IOException
     */
    public Socket createSocket(String host, int port) throws IOException {
        return factory.createSocket(host, port);
    }

    /**
     * Delegate method for {@link  javax.net.ssl.SSLSocketFactory#getDefaultCipherSuites() getDefaultCipherSuites()}
     *
     * @return
     */
    public String[] getDefaultCipherSuites() {
        return factory.getDefaultCipherSuites();
    }

    /**
     * Delegate method for {@link  javax.net.ssl.SSLSocketFactory#getSupportedCipherSuites() getSupportedCipherSuites()}
     *
     * @return
     */
    public String[] getSupportedCipherSuites() {
        return factory.getSupportedCipherSuites();
    }
}